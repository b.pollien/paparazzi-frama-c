(* file : auto_definition.ml *)

open Wp
open Lang
let rec lookup push select (e: Repr.repr) =
  let lookup_term push_l step_l term = 
        lookup push_l step_l (Repr.term term) in
  match e with
  | And es -> List.iter (lookup_term push select) es
  | Or es -> List.iter (lookup_term push select) es
  | Imply (hs,p) -> List.iter (lookup_term push select) (p::hs)
  | Call (f, terms) -> 
      let selection = (select (F.p_call f terms)) in 
      push (Auto.definition ~priority:2.0 selection)
  | _ -> ()



class auto_definition : Strategy.heuristic =
object
    method id = "Paparazzi.auto_definition"
    method title = "Auto definition"
    method descr = "Apply automatically definition tactic when it is possible"
    method search push sequent = (* heuristic code*)
      let p = snd sequent in
      let select = fun x -> (Tactical.(Clause(Goal x))) in
        lookup push select (Repr.pred p);
      Conditions.iter
            (fun step ->
            let p = Conditions.head step in
            let select = fun _ -> 
                    (Tactical.(Clause (Step step))) in 
            match step.condition with
            | Have _ -> lookup push select (Repr.pred p)
            | _ -> ()
            ) (fst sequent);
        
end

(* Register the stategy *)
let () = Strategy.register (new auto_definition)
