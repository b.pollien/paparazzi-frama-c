#!/bin/bash

# Prefix for the location of the frama-c binary
FRAMAC_PREFIX=""

# Temporary folder
TMP_FOLDER=".tmp_frama_c_analysis"
mkdir -p $TMP_FOLDER

# Temporary file for frama-c result
TMP_FILE_RES="$TMP_FOLDER/tmp_frama_c_analysis.txt "

# 2 temporary file to compute the list of functions to analyse
TMP1="$TMP_FOLDER/tmp_header_functions.txt"
TMP2="$TMP_FOLDER/tmp_preprocess_functions.txt" 

# Temporary file for preprocessed header file
TMP_HEADER="$TMP_FOLDER/tmp_preprocess.h"

# Temporary res of all result 
TMP_RES_GOALS="$TMP_FOLDER/tmp_res_goals.txt"

# All Frama-c variables
FRAMAC=$FRAMAC_PREFIX"frama-c"
FRAMACGUI=$FRAMAC_PREFIX"frama-c-gui"
EVA="-eva -lib-entry -main"
WP="-wp-fct"
WP_ARGS="-wp-cache update -wp-model real+Cast+ref -wp-prover alt-ergo,cvc4-strings-ce,z3,z3-ce,z3-nobv,coq,tip -wp-check-memory-model -wp-timeout 20 -wp-log r:"$TMP_FILE_RES
RTE="-rte -no-warn-left-shift-negative"

# Enable somke tests
if [ ! -z "${SMOKE}" ]; then
    echo "Smoke tests enable"
    WP_ARGS="$WP_ARGS -wp-smoke-tests"
fi

# To stop de script with ctrl+C
STOP_FC=0

#Variables for pprz
INCLUDE="-I../include"
DEFINE="-DFRAMA_C_ANALYSIS"
FLAGS="-cpp-extra-args=$INCLUDE -cpp-extra-args=$DEFINE"

# Files currently supported
FILES="math/pprz_algebra_double.c \
       math/pprz_algebra_float.c \
       math/pprz_algebra_int.c"

#FILES="math/pprz_algebra_int.c"

# Get all the functions name in $1 file.
# The result is written in $2 file.
get_function_names () {
    ctags -x --declaration --no-members $1  | grep -v -e "#define" -e "struct" | cut -f -1 -d " " > $2
}

for FILE in $FILES
do
    # Reset nb of goals
    echo "0 / 0" > $TMP_RES_GOALS

    # Get header file name
    HEADER=$(echo $FILE | sed 's/.$//')"h"

    # Get all functions declare in header file
    get_function_names $HEADER $TMP1

    # Pre-process the header and get all the function name
    gcc -E $INCLUDE -I. $DEFINE $HEADER > $TMP_HEADER
    get_function_names $TMP_HEADER $TMP2

    # Get all the functions by removing functions suppressed by 
    # the preprocessing. The Goals is to ignore function that can
    # not be verified by frama-c.

    FUNCTIONS=$(grep -Fxf $TMP1 $TMP2)

    for FUNCTION in $FUNCTIONS
    do
        # Reset to 0 and set to 1 after frama-c command
        # to detect ctrl+c 
        STOP_FC=0 
        echo -n "Analysing $FILE:$FUNCTION..."
        $FRAMAC $RTE $EVA $FUNCTION $WP $FUNCTION $WP_ARGS $FILE $FLAGS > /tmp/wp_analysis.txt 2>> /tmp/wp_analysis_error.txt || break
        STOP_FC=1
        echo -n -e "\\r\033[2K"
        ./output-frama-c-analysis.py $TMP_FILE_RES $FILE $FUNCTION $TMP_RES_GOALS
    done

    if [ $STOP_FC -eq 0 ]; then
        echo -e "\nFrama-c analysis stopped"
        break
    fi

    echo -n "WP analysis of $FILE : "
    cat $TMP_RES_GOALS
done

rm -rf $TMP_FOLDER
