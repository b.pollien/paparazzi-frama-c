#!/usr/bin/python3

import sys

def get_result_from_file(file):
    """
    Read the output file of the WP result 
    and return the number of goals proved and the total
    number of goals.

    WARNING: If the output format of WP change, 
             this function might be updated
    """
    f = open(file, "r")

    for line in f:
        # Find the line that display the result
        if "Proved goals:" in line:
            tab_el = line.split(" ")
            for i in range(len(tab_el)):
                # Find marker that separates the number of proved goals
                # and the total number of goals
                if tab_el[i] == "/":
                    return int(tab_el[i-1]), int(tab_el[i+1])

    raise ValueError("Number of proved goals not found ({}, {})! \n Verify that the file name is correct and that frama-c has not changed his output format for the result.".format(analysed_file, function_name))

    f.close()

if __name__ == '__main__':
    output_file = sys.argv[1]
    analysed_file = sys.argv[2]
    function_name = sys.argv[3]
    res_file = sys.argv[4]

    proved_goals, nb_goals = get_result_from_file(output_file)

    if (proved_goals != nb_goals):
        print("\033[31;01m[File Not Proved]\033[00m Function {} in {} file is not proved ({}/{})".format(function_name, analysed_file, proved_goals, nb_goals))

    # Get previous result
    f = open(res_file, "r")

    values = f.readline().split(" ")

    assert(values[1] == "/")
    proved_goals += int(values[0])
    nb_goals += int(values[2])

    f.close()

    # Write new result 
    f = open(res_file, "w")
    f.write("{} / {}\n".format(proved_goals, nb_goals))
    f.close()