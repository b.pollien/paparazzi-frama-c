#ifndef LIBC_FRAMA_C_H
#define LIBC_FRAMA_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>
#include <stdlib.h>

#define PI2 1.5707963

/****************************************************
 *  Redefinition of the contract of libC functions
 * **************************************************/

/*@ requires finite_arg: \is_finite(x);
    assigns \result \from x;
    ensures finite_result: \is_finite(\result);
    ensures result_domain: -1. <= \result <= 1.;
    ensures result_value: \result == \cos(x);
*/
extern float cosf(float x);

/*@ requires finite_arg: \is_finite(x);
    assigns \result \from x;
    ensures finite_result: \is_finite(\result);
    ensures result_domain: -1. <= \result <= 1.;
    ensures result_value: \result == \sin(x);
*/
extern float sinf(float x);

/*@ requires finite_args: \is_finite(x) && \is_finite(y);
    requires finite_logic_result: \is_finite(atan2f(x, y));
    assigns \result \from x, y;
    ensures finite_result: \is_finite(\result);
    ensures -PI2 <= \result <= PI2;
*/
extern float atan2f(float y, float x);

/*@ requires in_domain: \is_finite(x) && \abs(x) <= 1;
    assigns \result \from x;
    ensures finite_result: \is_finite(\result);
    ensures -PI2 <= \result <= PI2;
*/
extern float asinf(float x);

/*
typedef struct __fc_lldiv_t {
  long long int quot;              / Quotient.  /
  long long int rem;               / Remainder.  /
} lldiv_t;
*/

/*@ assigns \result \from numer, denom;
    ensures \result.quot == numer / denom;
    ensures \result.rem == numer % denom; */
extern lldiv_t lldiv(long long int numer, long long int denom);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif