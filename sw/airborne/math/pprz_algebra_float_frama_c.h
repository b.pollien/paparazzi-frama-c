#ifndef PPRZ_ALGEBRA_FLOAT_FRAMA_C_H
#define PPRZ_ALGEBRA_FLOAT_FRAMA_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include "libc_frama_c.h"

/*******************************
 *          INTERVAL
 * *****************************/

/*@
  predicate in_interval(float x, real x1, real x2) =
    \le_float((float) x1, x) && \le_float(x, (float) x2);
*/


/*******************************
 *          QUAT
 * *****************************/

/*@
  predicate unchange_quat{L}(struct FloatQuat *q) =
    \at(q->qi, L) == q->qi
    && \at(q->qx, L) == q->qx
    && \at(q->qy, L) == q->qy
    && \at(q->qz, L) == q->qz;
*/

/*******************************
 *          MATRIX
 * *****************************/

/*@
  predicate valid_size(int n) =
    0 <= n < INT_MAX;
*/

/* Validity of a matric of size n */
/*@
  predicate valid_float_mat(float **a, int n) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid(a[i] + (0..n-1)))
    && valid_size(n);
*/

/*@
  predicate rvalid_float_mat(float **a, int n) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid_read(a[i] + (0..n-1)))
    && valid_size(n);
*/

/* Validity of a matric of size m * n  */
/*@
  predicate valid_float_mat(float **a, int m, int n) =
    \valid_read(a + (0..m-1)) 
    && (\forall int i; 0 <= i < m ==> \valid(a[i] + (0..n-1)))
    && valid_size(n)
    && valid_size(m);
*/

/*@
  predicate rvalid_float_mat(float **a, int m, int n) =
    \valid_read(a + (0..m-1)) 
    && (\forall int i; 0 <= i < m ==> \valid_read(a[i] + (0..n-1)))
    && valid_size(n)
    && valid_size(m);
*/

/* Validity of the c columun for a matrix of size n */

/*@
  predicate valid_float_mat_col(float **a, int n, int c) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid(a[i] + c))
    && valid_size(n)
    && valid_size(c);
*/

/*@
  predicate rvalid_float_mat_col(float **a, int n, int c) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid_read(a[i] + c))
    && valid_size(n)
    && valid_size(c);
*/

/*******************************
 *          VECTOR
 * *****************************/


/* Validity of a matric of size n */
/*@
  predicate valid_float_vect(float *a, int n) =
    \valid(a + (0..n-1)) && valid_size(n);
*/

/*@
  predicate rvalid_float_vect(float *a, int n) =
    \valid_read(a + (0..n-1)) && valid_size(n);
*/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif