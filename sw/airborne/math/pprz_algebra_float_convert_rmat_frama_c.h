#ifndef PPRZ_ALGEBRA_FLOAT_CONVERT_RMAT_FRAMA_C_H
#define PPRZ_ALGEBRA_FLOAT_CONVERT_RMAT_FRAMA_C_H

#ifdef __cplusplus
extern "C" {
#endif

/*@
  lemma mul_sqrt_float:
    \forall float x;
    x >= 0 ==> x == \sqrt(x) * \sqrt(x);
*/

/*@
  lemma mul_sqrt_4:
    \forall real x;
    x >= 0 ==> \sqrt(4 * x * x) == 2 * x;
*/

/*******************************
 *    RealVect3 definition
 * *****************************/

/*@ ghost 
      struct RealVect3_s {float x;
                          float y;
                          float z;};
*/

//@ type RealVect3 = struct RealVect3_s;

//@ axiomatic Arbitrary_RealVect3 { logic RealVect3 empty_vect3;}

#define REALVECT3(vx, \
                  vy, \
                  vz) \
                  {{{empty_vect3 \
                  \with .x = (float) (vx)} \
                  \with .y = (float) (vy)} \
                  \with .z = (float) (vz)}


/*@
logic RealVect3 l_Vect_of_FloatVect3(struct FloatVect3 *v) =
  REALVECT3(v->x, v->y, v->z);
*/

/*@
logic RealVect3 mult_scalar(real scal, RealVect3 v) =
  REALVECT3(scal * v.x, scal * v.y, scal * v.z);
*/

/*@
  lemma mult_scalar_neutral:
    \forall RealVect3 v;
    mult_scalar(1.0, v) == v;
*/

/*@
logic RealVect3 neg_vect(RealVect3 v) =
  REALVECT3(-v.x, -v.y, -v.z);
*/

/*@
  lemma neg_vect_equi_mult_scalar:
    \forall RealVect3 v;
    neg_vect(v) ==  mult_scalar(-1.0, v);
*/

/*@
logic real scalar_product(RealVect3 v1, RealVect3 v2) =
  v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
*/

/*@
  lemma comm_scalar_product:
    \forall RealVect3 v1, v2;
    scalar_product(v1, v2) == scalar_product(v2, v1);
*/

/*@
logic RealVect3 add_RealVect3(RealVect3 v1, RealVect3 v2) =
  REALVECT3(
    v1.x + v2.x,
    v1.y + v2.y,
    v1.z + v2.z
  );
*/

/*@
  lemma comm_add_RealVect3:
    \forall RealVect3 v1, v2;
    add_RealVect3(v1, v2) == add_RealVect3(v2, v1);
*/

/*@
  lemma trans_add_RealVect3:
    \forall RealVect3 v1, v2, v3;
    add_RealVect3(v1, add_RealVect3(v2, v3)) == add_RealVect3(add_RealVect3(v1, v2), v3);
*/

/*@
logic RealVect3 cross_product(RealVect3 v1, RealVect3 v2) =
 REALVECT3(
   v1.y * v2.z - v1.z * v2.y,
   v1.z * v2.x - v1.x * v2.z,
   v1.x * v2.y - v1.y * v2.x
 );
*/

/*@
  lemma anticomm_cross_product:
    \forall RealVect3 v1, v2;
    cross_product(v1, v2) == mult_scalar(-1, cross_product(v2, v1));
*/

/*******************************
 *    RealRMat definition
 * *****************************/

#define SQR(a) ((a)*(a))

/*@ ghost 
      struct RealRMat_s {float a00; float a01; float a02;
                         float a10; float a11; float a12;
                         float a20; float a21; float a22;};
*/

//@ type RealRMat = struct RealRMat_s;

//@ axiomatic Arbitrary_RealRMat { logic RealRMat empty_rmat;}

#define REALRMAT(v00, v01, v02, \
                 v10, v11, v12, \
                 v20, v21, v22) \
                 {{{{{{{{{empty_rmat \
                 \with .a00 = (float) (v00)} \with .a01 = (float) (v01)} \with .a02 = (float) (v02)}\
                 \with .a10 = (float) (v10)} \with .a11 = (float) (v11)} \with .a12 = (float) (v12)}\
                 \with .a20 = (float) (v20)} \with .a21 = (float) (v21)} \with .a22 = (float) (v22)}

/*@ 
    logic RealRMat id_rmat = REALRMAT(1, 0, 0, 0, 1, 0, 0, 0, 1);
*/

/*@
logic RealRMat l_RMat_of_FloatRMat(struct FloatRMat *rmat) =
  REALRMAT(rmat->m[0], rmat->m[1], rmat->m[2],
       rmat->m[3], rmat->m[4], rmat->m[5],
       rmat->m[6], rmat->m[7], rmat->m[8]);
*/

/*******************************
 *    Matrix functions
 * *****************************/

/*@
  logic RealRMat transpose(RealRMat rmat) =
        REALRMAT(rmat.a00, rmat.a10, rmat.a20,
                 rmat.a01, rmat.a11, rmat.a21,
                 rmat.a02, rmat.a12, rmat.a22);
*/

/*@
  lemma transpose_transpose_rmat:
    \forall RealRMat rmat;
      transpose(transpose(rmat)) == rmat;
*/

/*@
  logic RealRMat mult_scalar(RealRMat rmat, integer a) =
        REALRMAT(a * rmat.a00, a * rmat.a01, a * rmat.a02,
                 a * rmat.a10, a * rmat.a11, a * rmat.a12,
                 a * rmat.a20, a * rmat.a21, a * rmat.a22);
*/

/*@
  lemma transpose_id:
    \forall integer a;
      transpose(mult_scalar(id_rmat, a)) == mult_scalar(id_rmat, a);
*/


/*@ 
  logic RealRMat mult_RealRMat(RealRMat rmat1, RealRMat rmat2) =
    REALRMAT(
      rmat1.a00 * rmat2.a00 + rmat1.a01 * rmat2.a10 
                                    + rmat1.a02 * rmat2.a20,
      rmat1.a00 * rmat2.a01 + rmat1.a01 * rmat2.a11 
                                    + rmat1.a02 * rmat2.a21,
      rmat1.a00 * rmat2.a02 + rmat1.a01 * rmat2.a12 
                                    + rmat1.a02 * rmat2.a22,
      rmat1.a10 * rmat2.a00 + rmat1.a11 * rmat2.a10 
                                    + rmat1.a12 * rmat2.a20,
      rmat1.a10 * rmat2.a01 + rmat1.a11 * rmat2.a11 
                                    + rmat1.a12 * rmat2.a21,
      rmat1.a10 * rmat2.a02 + rmat1.a11 * rmat2.a12 
                                    + rmat1.a12 * rmat2.a22,
      rmat1.a20 * rmat2.a00 + rmat1.a21 * rmat2.a10 
                                    + rmat1.a22 * rmat2.a20,
      rmat1.a20 * rmat2.a01 + rmat1.a21 * rmat2.a11 
                                    + rmat1.a22 * rmat2.a21,
      rmat1.a20 * rmat2.a02 + rmat1.a21 * rmat2.a12 
                                    + rmat1.a22 * rmat2.a22);
*/


/*@
  lemma mult_id_rmat_neutral:
    \forall RealRMat rmat;
      mult_RealRMat(rmat, id_rmat) == rmat;
*/

/*@
  lemma mult_rmat_id_neutral:
    \forall RealRMat rmat;
      mult_RealRMat(id_rmat, rmat) == rmat;
*/

/*@
logic RealVect3 mult_RealRMat_RealVect3(RealRMat rm, RealVect3 v) =
  REALVECT3(
    rm.a00 * v.x + rm.a01 * v.y + rm.a02 * v.z,
    rm.a10 * v.x + rm.a11 * v.y + rm.a12 * v.z,
    rm.a20 * v.x + rm.a21 * v.y + rm.a22 * v.z
  );
*/

/*@
  lemma mult_vect_id_neutral:
    \forall RealVect3 v;
      mult_RealRMat_RealVect3(id_rmat, v) == v;
*/


/*******************************
 *  Rotation matrix properties
 * *****************************/

/*@
  predicate rotation_matrix(RealRMat rmat) =
      mult_RealRMat(rmat, transpose(rmat)) == id_rmat;
*/

/*@
  logic real determinant(RealRMat rmat) = 
      rmat.a00 * rmat.a11 * rmat.a22
    + rmat.a02 * rmat.a10 * rmat.a21
    + rmat.a01 * rmat.a12 * rmat.a20
    - (  rmat.a02 * rmat.a11 * rmat.a20
       + rmat.a01 * rmat.a10 * rmat.a22
       + rmat.a00 * rmat.a12 * rmat.a21);
*/

/*@
  predicate special_orthogonal(RealRMat rmat) =
    determinant(rmat) == 1.0;
*/

/*
  lemma rotation_matrix_to_eq:
    \forall struct FloatRMat *rm;
    (\valid(rm)
    && (rotation_matrix(l_RMat_of_FloatRMat(rm))) ==> 
      (SQR(rm->m[0]) + SQR(rm->m[1]) + SQR(rm->m[2]) == 1.0
        && SQR(rm->m[3]) + SQR(rm->m[4]) + SQR(rm->m[5]) == 1.0
        && SQR(rm->m[6]) + SQR(rm->m[7]) + SQR(rm->m[8]) == 1.0
        && rm->m[0] * rm->m[3] + rm->m[1] * rm->m[4] + rm->m[2] * rm->m[5] == 0
        && rm->m[0] * rm->m[6] + rm->m[1] * rm->m[7] + rm->m[2] * rm->m[8] == 0
        && rm->m[3] * rm->m[6] + rm->m[4] * rm->m[7] + rm->m[5] * rm->m[8] == 0
      ));
*/

/*******************************
 *    RealQuat definition
 * *****************************/

/*@ ghost 
      struct RealQuat_s {float qi;
                         float qx;
                         float qy;
                         float qz;};
*/

//@ type RealQuat = struct RealQuat_s;

//@ axiomatic Arbitrary_RealQuat { logic RealQuat empty_quat;}

#define REALQUAT(vqi, \
                 vqx, \
                 vqy, \
                 vqz) \
                 {{{{empty_quat \
                 \with .qi = (float) (vqi)} \
                 \with .qx = (float) (vqx)} \
                 \with .qy = (float) (vqy)} \
                 \with .qz = (float) (vqz)}


/*@
logic RealQuat l_Quat_of_FloatQuat(struct FloatQuat *q) =
  REALQUAT(q->qi, q->qx, q->qy, q->qz);
*/

/*@
logic RealQuat conj(RealQuat q) =
  REALQUAT(q.qi, -q.qx, -q.qy, -q.qz);
*/

/*@
logic RealQuat conj(struct FloatQuat *q) =
  REALQUAT(q->qi, -q->qx, -q->qy, -q->qz);
*/

/*******************************
 *    RealQuatVect definition
 * *****************************/

/*@ ghost 
      struct RealQuatVect_s {float scalar;
                         struct RealVect3_s vect;};
*/

//@ type RealQuatVect = struct RealQuatVect_s;

//@ axiomatic Arbitrary_RealQuatVect { logic RealQuatVect empty_quatvect;}

#define REALQUATVECT(vqi, \
                 vqx, \
                 vqy, \
                 vqz) \
                 {{empty_quatvect \
                 \with .scalar = (float) (vqi)} \
                 \with .vect = ({{{empty_vect3 \
                  \with .x = (float) (vqx)} \
                  \with .y = (float) (vqy)} \
                  \with .z = (float) (vqz)})}

#define REALQUATVECT_FVECT(vqi, \
                           v) \
                          {{empty_quatvect \
                          \with .scalar = (float) (vqi)} \
                          \with .vect = (v)}

/*@
logic RealQuatVect l_QuatVect_of_FloatQuat(struct FloatQuat *q) =
  REALQUATVECT(q->qi, q->qx, q->qy, q->qz);
*/

/*@
logic RealQuat l_Quat_of_RealQuatVect(RealQuatVect q) =
  REALQUAT(q.scalar, q.vect.x, q.vect.y, q.vect.y);
*/

/*@
logic RealQuatVect l_QuatVect_of_RealQuat(RealQuat q) =
  REALQUATVECT(q.qi, q.qx, q.qy, q.qz);
*/

/*@
logic RealQuatVect mult_RealQuatVect(RealQuatVect q1, RealQuatVect q2) = 
  \let scalar = q1.scalar * q2.scalar - scalar_product(q1.vect, q2.vect);
  \let vect = add_RealVect3(add_RealVect3(
      mult_scalar(q1.scalar, q2.vect),
      mult_scalar(q2.scalar, q1.vect)),
      cross_product(q1.vect, q2.vect));
  REALQUATVECT_FVECT(scalar, vect);
*/

/*@
logic RealQuatVect conj_v(struct FloatQuat *q) =
  REALQUATVECT_FVECT(q->qi, REALVECT3(-q->qx, -q->qy, -q->qz));
*/

/*@
logic RealQuatVect conj_v(RealQuatVect q) =
  \let vect =  neg_vect(q.vect);
  REALQUATVECT_FVECT(q.scalar, vect);
*/

/*@
  lemma equi_conj:
    \forall struct FloatQuat *q;
    conj_v(q) 
        == l_QuatVect_of_RealQuat(conj(l_Quat_of_FloatQuat(q)));
*/

/*******************************
 *    CONVERT QUAT TO RMAT
 * *****************************/

/*@ 
  predicate unitary_quaternion(struct FloatQuat *q) = 
    SQR(q->qi) + SQR(q->qx) + SQR(q->qy) + SQR(q->qz) == 1;
*/

/*@ 
  predicate unitary_quaternion(RealQuat q) = 
    SQR(q.qi) + SQR(q.qx) + SQR(q.qy) + SQR(q.qz) == 1;
*/

/*@
  lemma equivalence_unitary_pred:
    \forall struct FloatQuat *q;
    unitary_quaternion(q) 
          <==> unitary_quaternion(l_Quat_of_FloatQuat(q));
*/

/*@
logic RealRMat l_RMat_of_FloatQuat(struct FloatQuat *q) =
  REALRMAT(SQR(q->qi) + SQR(q->qx) - SQR(q->qy) - SQR(q->qz),
       2 * (q->qx * q->qy - q->qz * q->qi),
       2 * (q->qx * q->qz + q->qy * q->qi),
       2 * (q->qx * q->qy + q->qz * q->qi),
       SQR(q->qi) - SQR(q->qx) + SQR(q->qy) - SQR(q->qz),
       2 * (q->qy * q->qz - q->qx * q->qi),
       2 * (q->qx * q->qz - q->qy * q->qi),
       2 * (q->qy * q->qz + q->qx * q->qi),
       SQR(q->qi) - SQR(q->qx) - SQR(q->qy) + SQR(q->qz));
*/

/*@
logic RealRMat l_RMat_of_FloatQuat_bis_1(struct FloatQuat *q) =
  REALRMAT(1 - 2 * (SQR(q->qy) + SQR(q->qz)), 
       2 * (q->qx * q->qy - q->qz * q->qi), 
       2 * (q->qx * q->qz + q->qy * q->qi), 
       2 * (q->qx * q->qy + q->qz * q->qi), 
       1 - 2 * (SQR(q->qx) + SQR(q->qz)), 
       2 * (q->qy * q->qz - q->qx * q->qi), 
       2 * (q->qx * q->qz - q->qy * q->qi), 
       2 * (q->qy * q->qz + q->qx * q->qi), 
       1 - 2 * (SQR(q->qx) + SQR(q->qy)));
*/
 
/*@
logic RealRMat l_RMat_of_FloatQuat_bis_2(struct FloatQuat *q) =
  REALRMAT(2 * (SQR(q->qi) + SQR(q->qx)) - 1,
       2 * (q->qx * q->qy - q->qz * q->qi),
       2 * (q->qx * q->qz + q->qy * q->qi),
       2 * (q->qx * q->qy + q->qz * q->qi),
       2 * (SQR(q->qi) + SQR(q->qy)) - 1,
       2 * (q->qy * q->qz - q->qx * q->qi),
       2 * (q->qx * q->qz - q->qy * q->qi),
       2 * (q->qy * q->qz + q->qx * q->qi),
       2 * (SQR(q->qi) + SQR(q->qz)) - 1);
*/

/*@
logic RealRMat l_RMat_of_FloatQuat(RealQuat q) =
  REALRMAT(SQR(q.qi) + SQR(q.qx) - SQR(q.qy) - SQR(q.qz),
       2 * (q.qx * q.qy - q.qz * q.qi),
       2 * (q.qx * q.qz + q.qy * q.qi),
       2 * (q.qx * q.qy + q.qz * q.qi),
       SQR(q.qi) - SQR(q.qx) + SQR(q.qy) - SQR(q.qz),
       2 * (q.qy * q.qz - q.qx * q.qi),
       2 * (q.qx * q.qz - q.qy * q.qi),
       2 * (q.qy * q.qz + q.qx * q.qi),
       SQR(q.qi) - SQR(q.qx) - SQR(q.qy) + SQR(q.qz));
*/


/*@
  lemma mutliple_def_rmat_of_quat_1:
    \forall struct FloatQuat *q;
    \valid_read(q) && unitary_quaternion(q)
      ==> l_RMat_of_FloatQuat(q) == l_RMat_of_FloatQuat_bis_1(q);
*/

/*@
  lemma mutliple_def_rmat_of_quat_2:
    \forall struct FloatQuat *q;
    \valid_read(q) && unitary_quaternion(q)
      ==> l_RMat_of_FloatQuat(q) == l_RMat_of_FloatQuat_bis_2(q);
*/

/*@
  lemma mutliple_def_rmat_of_quat_3:
    \forall struct FloatQuat *q;
    \valid_read(q) && unitary_quaternion(q)
      ==> l_RMat_of_FloatQuat_bis_1(q) == l_RMat_of_FloatQuat_bis_2(q);
*/

/*@
  lemma mutliple_def_rmat_of_quat_4:
    \forall struct FloatQuat *q;
    \valid_read(q) && unitary_quaternion(q)
      ==> l_RMat_of_FloatQuat(q) 
          == l_RMat_of_FloatQuat(l_Quat_of_FloatQuat(q));
*/

/*@
  lemma quat_of_rmat_ortho:
    \forall struct FloatQuat *q;
    unitary_quaternion(q)
    ==> rotation_matrix(l_RMat_of_FloatQuat(q));
*/

/*@
  lemma quat_of_rmat_special_ortho:
    \forall struct FloatQuat *q;
    unitary_quaternion(q)
    ==> special_orthogonal(l_RMat_of_FloatQuat(q));
*/

/*@
  logic RealQuatVect rotation_with_quat(RealQuatVect q, RealQuatVect v) =
   mult_RealQuatVect(mult_RealQuatVect(q, v), conj_v(q));
*/

/*@
  logic RealQuatVect rotation_with_quat(RealQuatVect q, RealVect3 v) =
  rotation_with_quat(q, REALQUATVECT_FVECT(0, v));
*/

/*@
  logic RealQuatVect rotation_with_quat(struct FloatQuat *q, RealVect3 v) =
    rotation_with_quat(l_QuatVect_of_FloatQuat(q), v);
*/

/*@
  lemma verify_rmat_of_quat_formula:
  \forall struct FloatQuat *q, struct FloatVect3 *v;
    \let vect = l_Vect_of_FloatVect3(v);
    \let rmat = l_RMat_of_FloatQuat(q);
    \let quat_rot = rotation_with_quat(q, vect);
        quat_rot == REALQUATVECT_FVECT(0, mult_RealRMat_RealVect3(rmat, vect));
*/

/*******************************
 *    CONVERT RMAT TO QUAT
 * *****************************/

/*@
  logic real trace(RealRMat rmat) = 
    rmat.a00 + rmat.a11 + rmat.a22;
*/

/*@
  logic real trace(struct FloatRMat *rmat) = 
    trace(l_RMat_of_FloatRMat(rmat));
*/


// ************* Positive trace ******************
/*@
  logic RealQuat l_FloatQuat_of_RMat_trace_pos(RealRMat rmat) =
    \let T = 1.0 + trace(rmat);
    \let S = 0.5 / \sqrt(T);
      REALQUAT(0.25 / S,
               (rmat.a21 - rmat.a12) * S,
               (rmat.a02 - rmat.a20) * S,
               (rmat.a10 - rmat.a01) * S);
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_trace_pos(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_trace_pos(l_RMat_of_FloatRMat(rmat));
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_trace_pos_t(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_trace_pos(transpose(l_RMat_of_FloatRMat(rmat)));
*/

// ************* A00 max ******************
/*@
  logic RealQuat l_FloatQuat_of_RMat_0_max(RealRMat rmat) =
    \let S = 2 * \sqrt( 1.0 + rmat.a00 - rmat.a11 - rmat.a22);
      REALQUAT((rmat.a21 - rmat.a12) / S,
               0.25 * S,
               (rmat.a01 + rmat.a10) / S,
               (rmat.a02 + rmat.a20) / S);
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_0_max(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_0_max(l_RMat_of_FloatRMat(rmat));
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_0_max_t(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_0_max(transpose(l_RMat_of_FloatRMat(rmat)));
*/

// ************* A11 max ******************
/*@
  logic RealQuat l_FloatQuat_of_RMat_1_max(RealRMat rmat) =
    \let S = 2 * \sqrt(1 - rmat.a00 + rmat.a11 - rmat.a22);
      REALQUAT((rmat.a02 - rmat.a20) / S,
               (rmat.a01 + rmat.a10) / S,
               0.25 * S,
               (rmat.a12 + rmat.a21) / S);
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_1_max(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_1_max(l_RMat_of_FloatRMat(rmat));
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_1_max_t(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_1_max(transpose(l_RMat_of_FloatRMat(rmat)));
*/

// ************* A22 max ******************
/*@
  logic RealQuat l_FloatQuat_of_RMat_2_max(RealRMat rmat) =
    \let S = 2 * \sqrt(1 - rmat.a00 - rmat.a11 + rmat.a22);
      REALQUAT((rmat.a10 - rmat.a01) / S,
               (rmat.a02 + rmat.a20) / S,
               (rmat.a12 + rmat.a21) / S,
               0.25 * S);
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_2_max(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_2_max(l_RMat_of_FloatRMat(rmat));
*/

/*@
  logic RealQuat l_FloatQuat_of_RMat_2_max_t(struct FloatRMat *rmat) = 
    l_FloatQuat_of_RMat_2_max(transpose(l_RMat_of_FloatRMat(rmat)));
*/

/*
  lemma impl_quat_rmat:
  \forall RealRMat rm, RealQuat q;
    trace(rm) > 0 && unitary_quaternion(q) && q.qi >= 0
    && q == l_FloatQuat_of_RMat_trace_pos(rm)
    ==>
    rm == l_RMat_of_FloatQuat(q);
*/

/*@
  lemma impl_rmat_quat_trace_pos:
  \forall RealRMat rm, RealQuat q;
    trace(rm) > 0
    && unitary_quaternion(q)
    && q.qi >= 0
    && rm == l_RMat_of_FloatQuat(q)
    ==> q == l_FloatQuat_of_RMat_trace_pos(rm);
*/

/*
  lemma verify_quat_of_rmat_pos_formula:
  \forall RealRMat rmat, RealVect3 vect;
    \let q = l_FloatQuat_of_RMat_trace_pos(rmat);
    \let quat_vect = l_QuatVect_of_RealQuat(q);
    \let quat_rot = rotation_with_quat(quat_vect, vect);
      trace(rmat) > 0 && rotation_matrix(rmat) ==> 
        quat_rot == REALQUATVECT_FVECT(0, mult_RealRMat_RealVect3(rmat, vect));
*/

/*@ lemma ne_zero_quat:
  \forall real a0, a1, a2, q;
    0 <= q
    && a1 < a0
    && a2 < a0
    && a0 + a1 + a2 <= 0
    && 1 - a2 - a1 + a0 == 4 * q * q
    ==> q != 0;
*/

/*@
  lemma impl_rmat_quat_a00_max:
  \forall RealRMat rm, RealQuat q;
    trace(rm) <= 0
    && rm.a00 > rm.a11
    && rm.a00 > rm.a22
    && unitary_quaternion(q) 
    && q.qx >= 0
    && rm == l_RMat_of_FloatQuat(q)
    ==> q == l_FloatQuat_of_RMat_0_max(rm);
*/

/*@
  lemma impl_rmat_quat_a11_max:
  \forall RealRMat rm, RealQuat q;
    trace(rm) <= 0
    && rm.a11 > rm.a00
    && rm.a11 > rm.a22
    && unitary_quaternion(q) 
    && q.qy >= 0
    && rm == l_RMat_of_FloatQuat(q)
    ==> q == l_FloatQuat_of_RMat_1_max(rm);
*/

/*@
  lemma impl_rmat_quat_a22_max:
  \forall RealRMat rm, RealQuat q;
    trace(rm) <= 0
    && rm.a22 > rm.a00
    && rm.a22 > rm.a11
    && unitary_quaternion(q) 
    && q.qz >= 0
    && rm == l_RMat_of_FloatQuat(q)
    ==> q == l_FloatQuat_of_RMat_2_max(rm);
*/


/***************************
 *      Trigo lemma
 ***************************/

 /*@
  lemma cos_sin_square:
    \forall real a;
    SQR(\cos(a)) + SQR(\sin(a)) == 1.0;
*/

/*@
 lemma float_rmat_of_eulers_321_1:
  \forall real a, b, c;
    SQR(\sin(a)) *  SQR(\cos(b)) 
      + SQR(\sin(a) * \sin(b) * \cos(c) - \sin(c) * \cos(a))
      + SQR(\cos(c) * \cos(a) + \sin(a) * \sin(b) * \sin(c)) == 1.0;
*/

/*@
 lemma float_rmat_of_eulers_321_2:
  \forall real a, b, c;
    SQR(\cos(a)) *  SQR(\cos(b)) 
      + SQR(\sin(c) * \sin(b) * \cos(a) - \sin(a) * \cos(c))
      + SQR(\sin(a) * \sin(c) + \sin(b) * \cos(a) * \cos(c)) == 1.0;
*/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif