#ifndef PPRZ_ALGEBRA_DOUBLE_FRAM_C_H
#define PPRZ_ALGEBRA_DOUBLE_FRAM_C_H

#ifdef __cplusplus
extern "C" {
#endif

/*******************************
 *          INTERVAL
 * *****************************/

/*@
  predicate in_interval(double x, real x1, real x2) =
    \le_double((double) x1, x) && \le_double(x, (double) x2);
*/

/*******************************
 *          DETERMINANT
 * *****************************/

/*@
  logic real determinant(struct DoubleRMat *rm) =
    rm->m[0] * rm->m[4] * rm->m[8]
    + rm->m[1] * rm->m[5] * rm->m[6]
    + rm->m[2] * rm->m[3] * rm->m[7]
    - rm->m[2] * rm->m[4] * rm->m[6]
    - rm->m[1] * rm->m[3] * rm->m[8]
    - rm->m[0] * rm->m[5] * rm->m[7];
*/

/*@
  predicate is_rotation_matrix(struct DoubleRMat *rm) = 
    determinant(rm) == 1;
*/

/*******************************
 *          LEMMA
 * *****************************/
/*
  lemma mul_double_bounded:
    \forall double x, y ;
        in_interval(x, -1.0, 1.0) && in_interval(y, -1.0, 1.0)
          ==>  in_interval(\mul_double(x, y), -1.0, 1.0);
*/

/*@
  lemma mul_double_bounded_full:
    \forall double x, y ;
        \is_finite(x) && \is_finite(y) && \le_double((double) -1.0, x) && \le_double(x, (double) 1.0) 
    && \le_double((double) -1.0, y) && \le_double(y, (double) 1.0) 
          ==>  \le_double((double) -1.0, \mul_double(x, y)) && \le_double(\mul_double(x, y), (double) 1.0) ;
*/

/*@
  lemma sub_double_finite:
    \forall double x, y ;
    in_interval(x, -1.0, 1.0) && in_interval(y, -1.0, 1.0) 
          ==> \is_finite(\sub_double(x, y));
*/

/*@
  lemma add_double_finite:
    \forall double x, y ;
    in_interval(x, -1.0, 1.0) && in_interval(y, -1.0, 1.0) 
          ==> \is_finite(\add_double(x, y));
*/

/*@
 lemma bounded_is_finite:
  \forall double x;
    in_interval(x, -1.0, 1.0) ==> \is_finite(x);
*/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif