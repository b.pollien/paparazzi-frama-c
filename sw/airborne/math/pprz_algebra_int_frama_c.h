#ifndef PPRZ_ALGEBRA_INT_FRAMA_C_H
#define PPRZ_ALGEBRA_INT_FRAMA_C_H

#ifdef __cplusplus
extern "C" {
#endif

#include <limits.h>

#include "libc_frama_c.h"

#define SQRT_INT_MAX 46340
// 46340 = sqrt(INT_MAX);

#define SQRT_INT_MAX2 32767
// 32767 = sqrt(INT_MAX/2)

#define SQRT_INT_MAX3 26754
// 26754 = SQRT(INT_MAX/3)

#define SQRT_INT_MAX4 23170
// 23170 = SQRT(INT_MAX/4)

#define MIN_QUAT (INT_MIN >> INT32_QUAT_FRAC)
#define MAX_QUAT (INT_MAX >> INT32_QUAT_FRAC)

#define MAX_RATE_FRAC (INT_MAX >> INT32_RATE_FRAC)

#define MIN_FREQ 50
#define MAX_32_QUAT (1 << INT32_QUAT_FRAC)
#define MAX_64_QUAT (1 << (INT32_QUAT_FRAC + 1))
#define MAX_32_RATE 40 * (1 << INT32_RATE_FRAC)
#define MAX_INTEGRATE_SUM (1 << (INT32_QUAT_FRAC + INT32_RATE_FRAC + 7))

/*
  |r| <= (INT_MAX) / (1 + 2 *((MAX_PPRZ_TRIG_INT * MAX_PPRZ_TRIG_INT) >> INT32_TRIG_FRAC))
  |r| <= INT_MAX / (2*MAX_PPRZ_TRIG_INT)
*/
#define MAX_321_OF_RATES_1 ((INT_MAX) \
                            / (1 + 2 * ( \
                            (MAX_PPRZ_TRIG_INT  * MAX_PPRZ_TRIG_INT \
                            >> INT32_TRIG_FRAC))))
#define MAX_321_OF_RATES_2 ((INT_MAX) \
                            / (2 * MAX_PPRZ_TRIG_INT))

/*
  |ed(psi|theta)| <= INT_MAX / (MAX_PPRZ_TRIG_INT)
  |ed(phi)| <= INT_MAX - (INT_MAX >> INT32_TRIG_FRAC)
*/
#define MAX_EULERS_321_1 (INT_MAX / MAX_PPRZ_TRIG_INT)
#define MAX_EULERS_321_2 (INT_MAX - (INT_MAX >> INT32_TRIG_FRAC) - 1)

#define MAX_QUAT_EULERS ((MAX_PPRZ_TRIG_INT  * MAX_PPRZ_TRIG_INT) >> INT32_TRIG_FRAC)

/*******************************
 *          INTERVAL
 * *****************************/

/*
  predicate in_interval(double x, real x1, real x2) =
    \le_double((double) x1, x) && \le_double(x, (double) x2);
*/

/*******************************
 *          DETERMINANT
 * *****************************/

/*
  logic real determinant(struct DoubleRMat *rm) =
    rm->m[0] * rm->m[4] * rm->m[8]
    + rm->m[1] * rm->m[5] * rm->m[6]
    + rm->m[2] * rm->m[3] * rm->m[7]
    - rm->m[2] * rm->m[4] * rm->m[6]
    - rm->m[1] * rm->m[3] * rm->m[8]
    - rm->m[0] * rm->m[5] * rm->m[7];
*/

/*
  predicate is_rotation_matrix(struct DoubleRMat *rm) = 
    determinant(rm) == 1;
*/

/*******************************
 *          LEMMA
 * *****************************/
/*@
  lemma mul_1_int64_bounded:
    \forall int64_t x, y ;
        -1 <= x <= 1 && -1 <= y <= 1
        ==> -1 <= x * y <= 1;
*/

/*@
  lemma mul_1_int32_bounded:
    \forall int32_t x, y ;
        -1 <= x <= 1 && -1 <= y <= 1
        ==> -1 <= x * y <= 1;
*/


/*@
  lemma mul_int32_bounded_min:
    \forall int32_t x, y;
      -SQRT_INT_MAX <= x <= SQRT_INT_MAX
      && -SQRT_INT_MAX <= y <= SQRT_INT_MAX
      ==> INT_MIN <= x * y;
*/

/*@
  lemma mul_int32_bounded_max:
    \forall int32_t x, y;
      -SQRT_INT_MAX <= x <= SQRT_INT_MAX
      && -SQRT_INT_MAX <= y <= SQRT_INT_MAX
      ==> x * y <= INT_MAX;
*/

/*@
 lemma add_int32_min:
   \forall int32_t a,b;
    INT_MIN / 2  <= a <= INT_MAX / 2
    && INT_MIN / 2 <= b <= INT_MAX / 2
    ==> INT_MIN <= a + b;
*/

/*@
 lemma add_int32_max:
   \forall int32_t a,b;
    INT_MIN / 2  <= a <= INT_MAX / 2
    && INT_MIN / 2 <= b <= INT_MAX / 2
    ==> a + b <= INT_MAX;
*/
/*
 lemma sub_int32_min:
   \forall int32_t a,b;
    INT_MIN / 2  <= a <= INT_MAX / 2
    && INT_MIN / 2 <= b <= INT_MAX / 2
    ==> INT_MIN <= a - b;
*/

/*
 lemma sub_int32_max:
   \forall int32_t a,b;
    INT_MIN / 2  <= a <= INT_MAX / 2
    && INT_MIN / 2 <= b <= INT_MAX / 2
    ==> a - b <= INT_MAX;
*/

/*******************************
 *          VECTOR
 * *****************************/

/*@
  predicate valid_size(int n) =
    0 <= n < INT_MAX;
*/

/* Validity of a matric of size n */
/*@
  predicate valid_int_vect(int *a, int n) =
    \valid(a + (0..n-1)) && valid_size(n);
*/

/*@
  predicate rvalid_int_vect(int *a, int n) =
    \valid_read(a + (0..n-1)) && valid_size(n);
*/

/*@
  predicate vect_int_bound(int *a, int n, integer bound) =
    \forall int i; 0 <= i < n ==> -bound <= a[i] <= bound;
*/

/*@
  predicate valid_int_vect(int *a, int n, integer bound) =
    \valid(a + (0..n-1)) && valid_size(n) && vect_int_bound(a, n, bound);
*/

/*@
  predicate rvalid_int_vect(int *a, int n, integer bound) =
    \valid_read(a + (0..n-1)) && valid_size(n) && vect_int_bound(a, n, bound);
*/

/*@
  predicate still_bound(int *a, int min, int max, integer bound) = 
    \forall int i; 
      min <= i < max
      ==> -bound <= a[i] <= bound;
*/

/*******************************
 *          MATRIX
 * *****************************/

/* Validity of a matric of size n */
/*@
  predicate valid_int_mat(int **a, int n) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid(a[i] + (0..n-1)))
    && valid_size(n);
*/

/*@
  predicate rvalid_int_mat(int **a, int n) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid_read(a[i] + (0..n-1)))
    && valid_size(n);
*/

/* Validity of a matric of size m * n  */
/*@
  predicate valid_int_mat(int **a, int m, int n) =
    \valid_read(a + (0..m-1)) 
    && (\forall int i; 0 <= i < m ==> \valid(a[i] + (0..n-1)))
    && valid_size(n)
    && valid_size(m);
*/

/*@
  predicate rvalid_int_mat(int **a, int m, int n) =
    \valid_read(a + (0..m-1)) 
    && (\forall int i; 0 <= i < m ==> \valid_read(a[i] + (0..n-1)))
    && valid_size(n)
    && valid_size(m);
*/

/*@
  predicate mat_int_bound(int **a, int m, int n, integer bound) =
  \forall int i, j;
    0 <= i < m && 0 <= j < n
    ==> -bound <= a[i][j] <= bound;
*/

/*@
  predicate valid_int_mat(int **a, int m, int n, int bound) =
    valid_int_mat(a, m, n) && mat_int_bound(a, m, n, bound);
*/

/*@
  predicate rvalid_int_mat(int **a, int m, int n, int bound) =
    rvalid_int_mat(a, m, n) && mat_int_bound(a, m, n, bound);
*/




/* Validity of the c columun for a matrix of size n */

/*@
  predicate valid_int_mat_col(int **a, int n, int c) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid(a[i] + c))
    && valid_size(n)
    && valid_size(c);
*/

/*@
  predicate rvalid_int_mat_col(int **a, int n, int c) =
    \valid_read(a + (0..n-1)) 
    && (\forall int i; 0 <= i < n ==> \valid_read(a[i] + c))
    && valid_size(n)
    && valid_size(c);
*/

/*******************************
 *         LEMMA for MAT_MUL
 * *****************************/

#define INT_MAX_n ((int) (INT_MAX/n))
#define SQRT_INTM_n ((int) \sqrt(INT_MAX_n))

/*@
  lemma sqrt_positive:
    \forall int a;
    a >= 0 ==> \sqrt(a) >= 0;
*/

/*
  lemma sqrt_le:
  \forall int a;
  a > 0 ==> \sqrt(a) <= a;
*/

/*@
  lemma sqrt_square_eq:
  \forall int a;
  a > 0 ==> a == \sqrt(a*a);
*/

/*@
  lemma square_sqrt_le:
  \forall int a;
  a > 0 ==> a >= ((int) \sqrt(a)) * ((int) \sqrt(a));
*/


/*@
  lemma square_sqrt_le_div:
  \forall int a, b;
  a > 0 && b > 0 ==> a/b >= ((int) \sqrt(a/b)) * ((int) \sqrt(a/b));
*/


/*@
    lemma square_sqrt_bound:
    \forall int a, b, bound;
      bound > 0
      && -bound <= a <= bound
      && -bound <= b <= bound
      ==> -bound*bound <= a * b <= bound * bound;
*/

/*@
  lemma mul_sqrt_max_n_bound:
    \forall int a, b, n;
      n > 0
      && -SQRT_INTM_n <= a <= SQRT_INTM_n
      && -SQRT_INTM_n <= b <= (int) \sqrt(INT_MAX/n)
      ==> -SQRT_INTM_n* SQRT_INTM_n <= a * b <= SQRT_INTM_n * SQRT_INTM_n;
*/

/*@
  lemma square_sqrt_le_max:
    \forall int n;
      n > 0
      ==> SQRT_INTM_n * SQRT_INTM_n <= INT_MAX_n ;
*/

/*@
  lemma n_mul_square_sqrt_le_n_max_n:
    \forall int n;
      n > 0
      ==> n * SQRT_INTM_n * SQRT_INTM_n <= n * INT_MAX_n ;
*/

/*@
  lemma max_n_le_max:
    \forall int n;
      n > 0
      ==> n * INT_MAX_n <= INT_MAX ;
*/

/*@
  lemma max_k_le_n_max:
    \forall int n, k;
      n > 0 && 0 < k < n
      ==> (k+1) * INT_MAX_n <= n * INT_MAX_n;
*/

/*@
  lemma max_k_n_le_max:
    \forall int n, k;
      n > 0 && 0 < k < n
      ==> (k+1) * INT_MAX_n <= INT_MAX ;
*/

/*@
  lemma n_mul_square_sqrt_le_max:
    \forall int n;
      n > 0
      ==> n * SQRT_INTM_n * SQRT_INTM_n <= INT_MAX ;
*/


/*@
  lemma square_sqrt_max_n_bound:
    \forall int a, b, n;
      n > 0
      && -SQRT_INTM_n <= a <= SQRT_INTM_n
      && -SQRT_INTM_n <= b <= SQRT_INTM_n
      ==> -((int) INT_MAX_n) <= a * b <= ((int) INT_MAX_n);
*/

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif